package com.jump.canvasdraw;


import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.poi.poiandroid.R;
import com.poi.word.ScrollControl;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.os.Handler;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

public class HandWrite extends View
{
    Paint paint = null;
    private Bitmap originalBitmap = null;
     Bitmap new1Bitmap = null;
//    private Bitmap new2Bitmap = null;
    private float clickX = 0,clickY = 0;
    private float startX = 0,startY = 0;
    private boolean isMove = true;
    private boolean isClear = false;
    int color = Color.RED;
     public float strokeWidth = 3.0f;
    private DrawControl drawing ;
    private Canvas mcanvas=null;
    
    private float drawx1=0,drawy1=0,drawx2=0,drawy2=0;
    
    private List <DrawPoint> drawP =null;
     
    private boolean isCon = false;
    private String cx,cy;

    public static boolean Writeable = false;;
    
	 public HandWrite(Context context) {
	        super(context);
	    }
	 
	public HandWrite(Context context, AttributeSet attrs, int defStyle) {
	        super(context, attrs, defStyle);
	    }
	public HandWrite(Context context, AttributeSet attrs)
	{
		super(context, attrs);

		originalBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.a).copy(Bitmap.Config.ARGB_8888, true);
		new1Bitmap = Bitmap.createBitmap(originalBitmap);
		if(drawP ==null) {drawP = new  ArrayList<DrawPoint>();}
	}
	
	public Matrix getmatrix(Bitmap bm){
		DisplayMetrics dm = new DisplayMetrics();
		//��ȡ��Ļ��Ϣ
		WindowManager wm = (WindowManager) getContext()
        .getSystemService(Context.WINDOW_SERVICE);
		int screenwidth = wm.getDefaultDisplay().getWidth();
		
		float t= (float) screenwidth/(float)bm.getWidth();		
		
		Matrix matrix = new Matrix(); 
		matrix.postScale(t,t);
        return matrix;
	}

    public void clear(){
    	isClear = true;
    	new1Bitmap = Bitmap.createBitmap(originalBitmap);
    	invalidate();
//        ByteArrayOutputStream newone = new ByteArrayOutputStream();
//        new2Bitmap.compress(Bitmap.CompressFormat.PNG,100,newone);
//        byte [] bitmapByte = newone.toByteArray();

    }
    public void setstyle(float strokeWidth){
    	this.strokeWidth = strokeWidth;
    }
	@Override
	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);

		Matrix m=getmatrix(new1Bitmap);
	    Bitmap resizeBmp = new1Bitmap;
		new1Bitmap = Bitmap.createBitmap(resizeBmp,0,0,resizeBmp.getWidth(),resizeBmp.getHeight(),m,true);

        mcanvas = canvas;
	    canvas.drawBitmap(HandWriting(new1Bitmap), 0,0 ,null);
		
	}

    public void drawtext (){
    String a =" jlfasdkjlkasjflkasjfdljdl";
    paint =new Paint();
    paint.setColor(0xFFFFFF);
        Rect rect = new Rect();
        paint.getTextBounds(a, 0, a.length(), rect);
        mcanvas.drawText(a, 20, rect.width(), paint);
        invalidate();
}

	public Bitmap HandWriting(Bitmap new1Bitmap)
	{
		Canvas canvas = null;

//		if(isClear){
//			Matrix m=getmatrix(new2Bitmap);
//			Bitmap resizeBmp = new2Bitmap;
//			new2Bitmap = Bitmap.createBitmap(resizeBmp,0,0,resizeBmp.getWidth(),resizeBmp.getHeight(),m,true);
//			canvas = new Canvas(new2Bitmap);
//		}
//		else{
			canvas = new Canvas(new1Bitmap);
//		}
		paint = new Paint();
		paint.setStyle(Style.STROKE);
		paint.setAntiAlias(true);
		paint.setColor(color);
		paint.setStrokeWidth(strokeWidth);

		
		if(isMove){
			//DrawSend(startX, startY, clickX, clickY);
//			canvas = new Canvas(new1Bitmap);
			canvas.drawLine(startX, startY, clickX, clickY, paint);
		}
		
		else if(isCon){
	 		String[] sx=cx.split(",");
	 		String[] sy=cy.split(",");
	 		float px,py,ax,ay;
	 		for(int i=1;i<sx.length;i++)
	 		{ px = Float.parseFloat(sx[i-1]); 
	 		  py = Float.parseFloat(sy[i-1]); 
	 		  ax = Float.parseFloat(sx[i]); 
	 		  ay = Float.parseFloat(sy[i]);
			  //mcanvas.drawBitmap(HandWriting(new1Bitmap), 0,0 ,null);
	 		 canvas.drawLine(px,py,ax,ay, paint);
			} 
	 		isCon =false;
		}
		
		startX = clickX;
		startY = clickY;
		
		if(isClear){
			return new1Bitmap;
		}
		return new1Bitmap;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		DrawPoint t = new DrawPoint(event.getX(),event.getY());
		drawP.add(t);
		
		clickX = event.getX();
		clickY = event.getY();
        if (!Writeable){
            long time = System.currentTimeMillis()-2;
                if((System.currentTimeMillis()-time)<3000) return true;
            else{
                    Toast.makeText(getContext(),"您无权限进行涂画",Toast.LENGTH_SHORT).show();
                    time = System.currentTimeMillis();
        }
        return true;
        }

		if(event.getAction() == MotionEvent.ACTION_DOWN){
			
			drawx1 = event.getX();
			drawy1 = event.getY();
			isMove = false;
			startX = clickX; 
			startY = clickY;
			
			invalidate();
			return true;
		}
		else if(event.getAction() == MotionEvent.ACTION_MOVE){
			
			isMove = true;
			invalidate();
			return true;
		}
		
		else 
			if(event.getAction() == MotionEvent.ACTION_UP){
			drawx2 = event.getX();
			drawy2 = event.getY();
			DrawSend(drawP);//发送封装的坐标
			drawP.clear();  }
		
		return super.onTouchEvent(event);
	}
   
   
	public void setNew1Bitmap(Bitmap b){
		new1Bitmap = b.copy(Bitmap.Config.ARGB_8888, true);
		
		originalBitmap = b.copy(Bitmap.Config.ARGB_8888, true);
		invalidate();
	}
	
	public void setListener(DrawControl t){
		drawing = t;
	}
	
	public void DrawSend(float sx,float sy,float cx,float cy){		
		drawing.drawsend(sx, sy, cx, cy);
	}
	
	public void DrawMove(float sx,float sy,float cx,float cy){		
		startX = sx; 
		startY = sy;
		clickX = cx;
		clickY = cy;
		//mcanvas.drawBitmap(HandWriting(new1Bitmap), 0,0 ,null);
		invalidate();
	}
	
	public void DrawMove(String s){		
        String ss[]= s.split("/");
		
		cx = ss[0];
		cy = ss[1];
		

 		isMove = false;
 		isClear = false;
 		isCon =true;
 		Handler handler =new Handler();
 		handler.post(mRunnable);
		
	}

	private  Runnable	mRunnable	= new Runnable() 
	{
		public void run()
		{
			postInvalidate(); }
	};
	
	public void DrawSend(List<DrawPoint> t){	
		String x="", y="";
		int fx,fy;		
        for(int i = 0;i < t.size(); i ++){
        	fx = (int) t.get(i).x;
        	fy = (int) t.get(i).y;
        	x=x+fx+",";
        	y=y+fy+",";
        	
        }
        String sum = x+"/"+y;
        drawing.drawsend(sum);       
	}
	

	
   class DrawPoint {
		public float x;
		public float y;
		
		DrawPoint(){x=0;y=0;}
		DrawPoint(float xx,float yy){
			x=xx;  y=yy;
		}
	}
	
}
