package com.jump.canvasdraw;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.control.ClientVoAdapter;
import com.example.network.SocketClient;
import com.example.network.SocketServer;
import com.poi.poiandroid.R;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class CanvasDrawActivity extends Activity {
    private static final String TAG = "CanvasDrawActivity";
    /**
     * Called when the activity is first created.
     */
    private int width;
    private int height;
    public static HandWrite handWrite = null;
    private Button clear = null;
    private int whichColor = 0;
    private int whichStrokeWidth = 0;

    private boolean isServer;
    static String x;
    static String y;

    //ͨ����socket;
    private SocketClient client = null;
    private SocketServer server = null;

    DrawControl DrawListener = new DrawControl() {

        //无用到
        @Override
        public void drawsend(float sx, float sy, float cx, float cy) {
            // TODO Auto-generated method stub
            if (isServer) {
                String s = sx + "," + sy + "," + cx + "," + cy;
                server.sendMsg("drawmove:" + s);
            }
        }

        @Override
        public void drawsend(String s) {
            // TODO Auto-generated method stub
            if (isServer) {
                //������������Ϣ���ͻ���"drawmove:" + s
                server.sendMsg("drawmove:" + s);
            } else {
                //�ͻ��˷�����Ϣ��������
                client.sendMsg("drawmove:" + s);
            }
        }


    };
    private Map<SocketServer.ClientVo, Boolean> clientVoBooleanMap = new Hashtable<>();


    public static void drawControl(String s) {
        /*
         String[] c=s.split(",");
 		float[] c2=new float[4];
 		for(int i=0;i<c.length;i++)
 		{ 
 			c2[i] = Float.parseFloat(c[i]); 
 		}
		handWrite.DrawMove(c2[0], c2[1], c2[2], c2[3]);
		*/

        handWrite.DrawMove(s);
		/*
		String ss[]= s.split("/");
		
		 x = ss[0];
		 y = ss[1];
		
 		String[] sx=x.split(",");
 		String[] sy=y.split(",");
 		float fx1,fy1,fx2,fy2;
 		for(int i=1;i<sx.length;i++)
 		{ fx1 = Float.parseFloat(sx[i-1]); 
 		  fy1 = Float.parseFloat(sy[i-1]); 
 		  fx2 = Float.parseFloat(sx[i]); 
		  fy2 = Float.parseFloat(sy[i]);
		handWrite.DrawMove(fx1,fy1,fx2,fy2);
		mHandler.post(mRunnable);
		} */
    }

    public static void clearControl(){
        handWrite.clear();
    }

    public static void setblue(){
        handWrite.color = Color.BLACK;
    }

    public static void setgreen(){
        handWrite.color = Color.GREEN;
    }

    public static void setred(){
        handWrite.color = Color.RED;
    }

    public static void writetext(){
        handWrite.drawtext();
    }

    /*
    public static void drawControl(String x,String y){

         String[] sx=x.split(",");
         String[] sy=x.split(",");
         float fx1,fy1,fx2,fy2;
         for(int i=1;i<sx.length;i++)
         { fx1 = Float.parseFloat(sx[i-1]);
           fy1 = Float.parseFloat(sy[i-1]);
           fx2 = Float.parseFloat(sx[i]);
          fy2 = Float.parseFloat(sy[i]);
        handWrite.DrawMove(fx1,fy1,fx2,fy2);
        }
    }
    */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        //	    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.main);
        getSocket();

        Intent intent = getIntent();
        Bitmap bitmap = null;
        if (intent != null) {
            byte[] bis = intent.getByteArrayExtra("bitmap");
            bitmap = BitmapFactory.decodeByteArray(bis, 0, bis.length);
        }

        handWrite = (HandWrite) findViewById(R.id.handwriteview);
        handWrite.setListener(DrawListener);
        handWrite.setNew1Bitmap(bitmap);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        menu.add(0, 1, 1, "清屏");
        menu.add(0, 2, 2, "画笔颜色");
        menu.add(0, 3, 3, "画笔粗细");
        menu.add(0, 5, 5, "权限管理");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        if (item.getItemId() == 4) {

            File f = new File(Environment.getExternalStorageDirectory()
                    .getAbsolutePath() + "/aaa.jpg");
            try {

                saveMyBitmap(f, handWrite.new1Bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }


        } else if (item.getItemId() == 1) {

            handWrite.clear();
            if(isServer)
            {
                server.sendMsg("clearall");
            }
            else    client.sendMsg("clearall");


        } else if (item.getItemId() == 2) {

            Dialog mDialog = new AlertDialog.Builder(CanvasDrawActivity.this)
                    .setTitle("画笔颜色")
                    .setSingleChoiceItems(new String[]{"红色", "绿色", "黑色"}, whichColor, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            switch (which) {
                                case 0: {

                                    handWrite.color = Color.RED;
                                    whichColor = 0;
                                    if(isServer)
                                    {
                                        server.sendMsg("redcolor");
                                    }
                                    else    client.sendMsg("redcolor");
                                    break;
                                }
                                case 1: {

                                    handWrite.color = Color.GREEN;
                                    whichColor = 1;
                                    if(isServer)
                                    {
                                        server.sendMsg("greencolor");
                                    }
                                    else    client.sendMsg("greencolor");
                                    break;
                                }
                                case 2: {

                                    handWrite.color = Color.BLACK;
                                    whichColor = 2;
                                    if(isServer)
                                    {
                                        server.sendMsg("blackcolor");
                                    }
                                    else    client.sendMsg("blackcolor");
                                    break;
                                }
                            }
                        }
                    })
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                        }
                    })
                    .create();
            mDialog.show();


        } else if (item.getItemId() == 3) {


            Dialog mDialog = new AlertDialog.Builder(CanvasDrawActivity.this)
                    .setTitle("画笔粗细")
                    .setSingleChoiceItems(new String[]{"细", "中", "粗"}, whichStrokeWidth, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            switch (which) {
                                case 0: {

                                    handWrite.strokeWidth = 3.0f;
                                    whichStrokeWidth = 0;
                                    if(isServer)
                                    {
                                        server.sendMsg("Width1");
                                    }
                                    else   { client.sendMsg("Width1");}
                                    break;
                                }
                                case 1: {

                                    handWrite.strokeWidth = 6.0f;
                                    whichStrokeWidth = 1;
                                    if(isServer)
                                    {
                                        server.sendMsg("Width2");
                                    }
                                    else   { client.sendMsg("Width2");}
                                    break;
                                }
                                case 2: {

                                    handWrite.strokeWidth = 9.0f;
                                    whichStrokeWidth = 2;
                                    if(isServer)
                                    {
                                        server.sendMsg("Width3Width3");
                                    }
                                    else   { client.sendMsg("Width3");}
                                    break;
                                }
                            }
                        }
                    })
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                        }
                    })
                    .create();
            mDialog.show();


        }
        else if (item.getItemId() == 5) {
            final View parentView = getLayoutInflater().inflate(R.layout.control_listview, null);
            final ListView lv =(ListView)parentView.findViewById(R.id.control_listview);
            final List<SocketServer.ClientVo> clientlist = SocketServer.getclientvolist();
            final ClientVoAdapter clientVoAdapter = new ClientVoAdapter(this, clientlist, R.layout.listview_container,clientVoBooleanMap);
            lv.setAdapter(clientVoAdapter);

            final AlertDialog mDialog = new AlertDialog.Builder(this)
                    .setTitle("画笔授权")
                    .setView(parentView)
                    .setNegativeButton("取消", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface mDialog, int which) {
                            // TODO Auto-generated method stub
                            mDialog.dismiss();
                        }
                    })
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
//                            lv.(new AdapterView.OnItemClickListener() {
//                                @Override
//                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                                    SocketServer.ClientVo current = clientlist.get(position);
//                                    SocketServer.mPrintWriterMap.get(current).print("WritePower");
//                                    SocketServer.mPrintWriterMap.get(current).flush();
//                                }
//                            });

                            clientVoBooleanMap = clientVoAdapter.getPowercheck();
                            for (SocketServer.ClientVo clientVo : clientVoBooleanMap.keySet()) {
                                if (clientVoBooleanMap.get(clientVo) == true) {
                                    SocketServer.mPrintWriterMap.get(clientVo).print("WritePower");
                                    SocketServer.mPrintWriterMap.get(clientVo).flush();
                                } else if (clientVoBooleanMap.get(clientVo) == false) {
                                    SocketServer.mPrintWriterMap.get(clientVo).print("WritePowerCancel");
                                    SocketServer.mPrintWriterMap.get(clientVo).flush();
                                }
                            }

                            dialog.dismiss();
                        }
                    })
                    .create();
//            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();


        }
        return super.onOptionsItemSelected(item);
    }


    public void saveMyBitmap(File f, Bitmap mBitmap) throws IOException {
        try {
            f.createNewFile();
            FileOutputStream fOut = null;
            fOut = new FileOutputStream(f);
            mBitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getSocket() {
        Intent intent = getIntent();
        String type = intent.getStringExtra("type");
        if (type.equals("client")) {
            client = SocketClient.getInstance();
            isServer = false;
        }

        if (type.equals("server")) {
            server = SocketServer.getInstance();
            isServer = true;
        }
    }



}