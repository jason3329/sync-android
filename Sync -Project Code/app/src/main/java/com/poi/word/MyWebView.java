package com.poi.word;


import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.WebView;


class MyWebView extends WebView{
	private ScrollControl ScrollListener ;
	
	   public MyWebView(Context context) {
	        super(context);
	    }
	 
	    public MyWebView(Context context, AttributeSet attrs, int defStyle) {
	        super(context, attrs, defStyle);
	    }
	 
	    public MyWebView(Context context, AttributeSet attrs) {
	        super(context, attrs);
	    }

	public void setListener(ScrollControl t){
		ScrollListener = t;
	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		// TODO Auto-generated method stub
		super.onScrollChanged(l, t, oldl, oldt);
    	int y=t;
	    Log.i("坐标y",y+"");	
	    
	    ScrollListener.ScrollChanged(y);
	}
	
	}