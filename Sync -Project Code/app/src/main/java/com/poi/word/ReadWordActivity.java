package com.poi.word;
 

import java.io.BufferedWriter;
 
import java.io.ByteArrayOutputStream;
 
import java.io.File;
 
import java.io.FileInputStream;
 
import java.io.FileNotFoundException;
 
import java.io.FileOutputStream;
 
import java.io.IOException;
 
import java.io.OutputStreamWriter;
 
import java.util.List;
 

import javax.xml.parsers.DocumentBuilderFactory;
 
import javax.xml.parsers.ParserConfigurationException;
 
import javax.xml.transform.OutputKeys;
 
import javax.xml.transform.Transformer;
 
import javax.xml.transform.TransformerException;
 
import javax.xml.transform.TransformerFactory;
 
import javax.xml.transform.dom.DOMSource;
 
import javax.xml.transform.stream.StreamResult;
 


import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.HWPFDocumentCore;
 
import org.apache.poi.hwpf.converter.PicturesManager;
import org.apache.poi.hwpf.converter.WordToHtmlUtils;
 
import org.apache.poi.hwpf.converter.WordToHtmlConverter;
 
import org.apache.poi.hwpf.usermodel.Picture;
 
import org.apache.poi.hwpf.usermodel.PictureType;
 
import org.w3c.dom.Document;

import com.example.gproject.MyListener;
import com.example.network.SocketClient;
import com.example.network.SocketServer;
import com.poi.poiandroid.R;



import android.net.Uri;
import android.os.Bundle;
 
import android.app.Activity;
import android.content.Intent;
 
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
 
import android.webkit.WebView;
import android.widget.TextView;
 

public class ReadWordActivity extends Activity {

    private String docPath ;
 
    private String docName ;
 
    private String savePath = "/mnt/sdcard/FeigeRec/cache/";    
    
    private static MyWebView webView;
	//标志为client或server
	private boolean isServer ; 
    
	//通信用socket;
	private SocketClient client = null ;
	private SocketServer server = null;

	ScrollControl ScrollListener = new ScrollControl(){

		@Override
		public void ScrollChanged(int ty) {
			// TODO Auto-generated method stub
	    	int y=ty;
			if(isServer)
			{ server.sendMsg("scroll:"+y); }
		}

		 		 
	};    
 
    @Override
 
    public void onCreate(Bundle savedInstanceState) {
 
        super.onCreate(savedInstanceState);
 
        setContentView(R.layout.activity_word);
        getSocket();
        
        Intent i = getIntent();
        Uri uri = i.getData();
        String path = uri.getPath();
        docPath = path.substring(0, path.lastIndexOf("/")+1);
        docName= i.getStringExtra("filename");
        String name = docName.substring(0, docName.indexOf("."));

        
        webView = (MyWebView) findViewById(R.id.officeword); 
        webView.setListener(ScrollListener);
 
        try {
 
            if(!(new File(savePath+name).exists()))
 
                new File(savePath+name).mkdirs();
 
            convert2Html(docPath+docName,savePath+name+".html");
 
        } catch (Exception e){
 
            e.printStackTrace();
 
        }
 
        //WebView加载显示本地html文件
 
      
        WebSettings webSettings = webView.getSettings();
        webSettings.setLoadWithOverviewMode(true);    
        webSettings.setSupportZoom(true);
        webSettings.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setBuiltInZoomControls(true);
        //webSettings.setUseWideViewPort(true);
        //webView.setInitialScale(50);
        webView.loadUrl("file:/"+savePath+name+".html");
        //listScrollDown();
 
    }
 
    
    /**
 
     * word文档转成html格式 
     * */
 
    public void convert2Html(String fileName, String outPutFile)  
 
            throws TransformerException, IOException,  
 
            ParserConfigurationException {  
 
        HWPFDocument wordDocument = new HWPFDocument(new FileInputStream(fileName));
         WordToHtmlConverter wordToHtmlConverter = new WordToHtmlConverter(
        DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());
        
 
        //设置图片路径
 
        wordToHtmlConverter.setPicturesManager(new PicturesManager()  
 
         {  
 
             public String savePicture( byte[] content,  
 
                     PictureType pictureType, String suggestedName,  
 
                     float widthInches, float heightInches )  
 
             {  
 
                 String name = docName.substring(0,docName.indexOf("."));
 
                 return name+"/"+suggestedName;  
 
             }  
 
         } );
 
    
        //保存图片
 
       List<Picture> pics=wordDocument.getPicturesTable().getAllPictures();  
 
        if(pics!=null){  
 
            for(int i=0;i<pics.size();i++){  
 
                Picture pic = (Picture)pics.get(i);  
 
                System.out.println( pic.suggestFullFileName()); 
                try {  
 
                    String name = docName.substring(0,docName.indexOf("."));
 
                    pic.writeImageContent(new FileOutputStream(savePath+ name + "/"
 
                            + pic.suggestFullFileName()));
 
                } catch (FileNotFoundException e) {  
 
                    e.printStackTrace();  
 
                }    
            }  
 
        } 
 
        wordToHtmlConverter.processDocument(wordDocument);
 
        Document htmlDocument = wordToHtmlConverter.getDocument();  
 
        ByteArrayOutputStream out = new ByteArrayOutputStream();
 
        DOMSource domSource = new DOMSource(htmlDocument);
 
        StreamResult streamResult = new StreamResult(out);
 
  
 
        TransformerFactory tf = TransformerFactory.newInstance();  
 
        Transformer serializer = tf.newTransformer();  
 
        serializer.setOutputProperty(OutputKeys.ENCODING, "utf-8");  
 
        serializer.setOutputProperty(OutputKeys.INDENT, "yes");  
 
        serializer.setOutputProperty(OutputKeys.METHOD, "html");
 
        serializer.transform(domSource, streamResult);  
 
        out.close();  
 
        //保存html文件
 
        writeFile(new String(out.toByteArray()), outPutFile); 
    }
 
    
    /**
 
     * 将html文件保存到sd卡
 
     * */
 
    public void writeFile(String content, String path) {  
 
        FileOutputStream fos = null;  
 
        BufferedWriter bw = null;  
 
        try {  
 
            File file = new File(path);  
 
            if(!file.exists()){
 
                file.createNewFile();
 
            }                
            fos = new FileOutputStream(file);  
 
            bw = new BufferedWriter(new OutputStreamWriter(fos,"utf-8"));  
 
            bw.write(content);  
 
        } catch (FileNotFoundException fnfe) {  
 
            fnfe.printStackTrace();  
 
        } catch (IOException ioe) {  
 
            ioe.printStackTrace();  
 
        } finally {  
 
            try {  
 
                if (bw != null)  
 
                    bw.close();  
 
                if (fos != null)  
 
                    fos.close();  
 
            } catch (IOException ie) {  
 
            }  
 
        }  
 
    }
 

    /*
    public void showsimpleWord() {
        File file = new File("/sdcard/1.doc");

        HWPFDocumentCore wordDocument = null;
        try {
            wordDocument = WordToHtmlUtils.loadDoc(new FileInputStream(file));
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        WordToHtmlConverter wordToHtmlConverter = null;
        try {
            wordToHtmlConverter = new WordToHtmlConverter(
                    DocumentBuilderFactory.newInstance().newDocumentBuilder()
                            .newDocument());
            wordToHtmlConverter.processDocument(wordDocument);
            org.w3c.dom.Document htmlDocument = wordToHtmlConverter
                    .getDocument();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            DOMSource domSource = new DOMSource(htmlDocument);
            StreamResult streamResult = new StreamResult(out);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer serializer = tf.newTransformer();
            serializer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            serializer.setOutputProperty(OutputKeys.INDENT, "yes");
            serializer.setOutputProperty(OutputKeys.METHOD, "html");
            serializer.transform(domSource, streamResult);
            out.close();
            String result = new String(out.toByteArray());
            System.out.println(result);
            webView.loadData(result,"text/html", "utf-8");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }  */
    
 

	public static void ScrollTo(int y)   //PageNum为页号;
	{
		webView.scrollTo(0, y);
	}
	
	public void getSocket(){
		Intent intent = getIntent();  
		String type = intent.getStringExtra("type");
		if(type.equals("client")) 
		{ 
			client = SocketClient.getInstance();
			isServer= false ;
		}
		
		if(type.equals("server")) 
		{ 
			server = SocketServer.getInstance();
			isServer= true;
		}
	}
  
    
}
