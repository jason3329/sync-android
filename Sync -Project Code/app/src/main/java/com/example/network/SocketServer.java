package com.example.network;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.example.gproject.MyListener;
import com.example.gproject.ServerActivity;
import com.jump.canvasdraw.CanvasDrawActivity;
import com.poi.poiandroid.DrawBegin;


import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class SocketServer {
    // ////////////////////////////////////////////////////////////////////////////////////    
//服务器端口 -> 默认通信端口号为8888；
    public static int SERVERPORT = 8888;
    //存储所有客户端Socket连接对象
    private static List<Socket> mClientList = new ArrayList<Socket>();
    //线程池
    private ExecutorService mExecutorService;
    //ServerSocket对象
    private ServerSocket mServerSocket;
    //开启服务器


    private ServerSocket serverSocket = null;
    private List<Socket> mSocketServer;
    private List<BufferedReader> mBufferedReaderServer;
    private List<PrintWriter> mPrintWriterServer;

    public static Map<ClientVo, PrintWriter> mPrintWriterMap = new Hashtable<>();


    public static ArrayList<String> Nickname = new ArrayList<>();
    private String recvText;
    private String recvMsgServer;
    private DrawBegin drawbegin;
//    public static Map<String, SocketServer> Portmap = new Hashtable<>();

    private List<Thread> mThreadServer;

    private boolean serverRuning = false;

    private MyListener viewListener;
    private int n = 0;

    private static SocketServer ServerInstance = null;  //设置为全局静态变量;
    private PrintWriter mWriterServer;

    public static List<ClientVo> getclientvolist(){
        List<ClientVo> clientVoList = new ArrayList<>();
        for (ClientVo clientVo : mPrintWriterMap.keySet()){
            clientVoList.add(clientVo);
        }
        return clientVoList;
    }

    public static SocketServer getInstance() {
        if (ServerInstance != null) {
            return ServerInstance;
        }

        return null;
    }

    public void showport() {
        String i = SERVERPORT + "";
        Log.i("port", i);
    }


    //构造函数;
    public SocketServer(MyListener myListener) {
        // TODO Auto-generated constructor stub
        viewListener = myListener;
        ServerInstance = SocketServer.this;
        mThreadServer = new ArrayList<Thread>();
        mSocketServer = new ArrayList<Socket>();
        mBufferedReaderServer = new ArrayList<BufferedReader>();
        mPrintWriterServer = new ArrayList<PrintWriter>();
    }

    public void setDrawListner(DrawBegin d) {
        drawbegin = d;
    }

    class mcreateServerRunnable implements Runnable {

        @Override
        public void run() {
            BufferedReader mReaderServer;
            Socket m_SocketServer;


            try {
                if (serverSocket == null) {
                    serverSocket = new ServerSocket(SERVERPORT);
                }
                SocketAddress address = null;
                if (!serverSocket.isBound()) {
                    serverSocket.bind(address, 0);
                }

                //方法用于等待客服连接 
                m_SocketServer = serverSocket.accept();

                //接受客服端数据BufferedReader对象
                mReaderServer = new BufferedReader(new InputStreamReader(m_SocketServer.getInputStream()));
                //给客服端发送数据
                mWriterServer = new PrintWriter(m_SocketServer.getOutputStream(), true);
                //mPrintWriter.println("服务端已经收到数据！");

                //添加到list中;

                mBufferedReaderServer.add(mReaderServer);
                mPrintWriterServer.add(mWriterServer);
                mSocketServer.add(m_SocketServer);


                //当连接成功后，创建多一个新的线程以监听其它客户端;
                Message msg = new Message();
                msg.what = 1;
                mHandler.sendMessage(msg);


            } catch (IOException e) {
                // TODO Auto-generated catch block
                //e.printStackTrace();
                Message msg = new Message();
                msg.what = 0;
                //mHandler.sendMessage(msg);
                return;
            }
            char[] buffer = new char[256];
            int count = 0;


            while (serverRuning) {
                try {

                    //if( (recvMessageServer = mBufferedReaderServer.readLine()) != null )//获取客服端数据
                    if ((count = mReaderServer.read(buffer)) > 0) {
                        recvMsgServer = getInfoBuff(buffer, count);  //接收客户端发送的信息;
                        Message msg = new Message();
                        msg.what = 0;
                        mHandler.sendMessage(msg);
                    } else {

                    }
                } catch (Exception e) {
                    recvMsgServer = "接收异常:" + e.getMessage() + "\n";//消息换行
                    Message msg = new Message();
                    msg.what = 0;
                    mHandler.sendMessage(msg);
                    return;
                }
            }
        }

    }


    public void createServer() {
        // TODO Auto-generated method stub
        if (serverRuning) {
            serverRuning = false;
            /*
            try {
				
				if(serverSocket!=null)
				{
					serverSocket.close();
					serverSocket = null;
				}
				if(mSocketServer!=null)
				{
					mSocketServer.close();
					mSocketServer = null;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} */

            interruptThread();

        } else {
            serverRuning = true;
            createServerThread();

        }
    }


    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            //刷新
            if (msg.what == 0) {
                try {
                    String message = recvMsgServer;
                    Log.i("is", "here");
                    //若为客户端角色信息的消息
                    if (message.contains("IP")) {
                        ClientVo cv1 = new ClientVo();
                        String i[] = message.split("@"); //截断消息
                        String k[] = i[0].split(":");//处理ip信息
                        cv1.setIp(k[1]);//保存该客户端的ip地址
                        String j[] = i[1].split(":");
                        cv1.setName(j[1]);//保存该客户端的名称
                        Nickname.add(n, j[1]);//显示至服务器已连接主界面
                        ServerActivity.showname(n, j[1]);
                        n++;
                        mPrintWriterMap.put(cv1,mWriterServer);//将信息与输入流配对保存至Hashmap
                        return;
                    }

//                    if (message.contains("Newport")) {
//                        String i[] = message.split(":");
//                        Portmap.put(i[1], null);
//                        return;
//                    }
                    if (message.equals("clearall")) {
                        CanvasDrawActivity.clearControl();
                        return;
                    } else if (message.equals("blackcolor")) {
                        CanvasDrawActivity.setblue();
                        return;
                    } else if (message.equals("greencolor")) {
                        CanvasDrawActivity.setgreen();
                        return;
                    } else if (message.equals("redcolor")) {
                        CanvasDrawActivity.setred();
                        return;
                    } else if (message.equals("Width1")) {
                        CanvasDrawActivity.handWrite.strokeWidth = 3.0f;
                        return;
                    } else if (message.equals("Width2")) {
                        CanvasDrawActivity.handWrite.strokeWidth = 6.0f;
                        return;
                    } else if (message.equals("Width3")) {
                        CanvasDrawActivity.handWrite.strokeWidth = 9.0f;
                        return;
                    } else if (!message.contains(":")) {
                        viewListener.refreshActivity(recvMsgServer);
                        return;
                    } else {
                        String s[] = message.split(":");

                        //
                        if (s[0].equals("drawmove")) {
                            String s3 = s[s.length - 1];
                            CanvasDrawActivity.drawControl(s3);
                            sendMsg("drawmove:" + message);
                        }
                        if (s[0].equals("drawbegin")) {
                            drawbegin.beginDraw();
                            sendMsg("drawbeign"+message);

                        }
                    }
                } catch (Exception e) {
                    Log.e("监听出错", e.toString());
                }
            }

            if (msg.what == 1) {
                createServerThread();
            }
        }
    };

    private String getInfoBuff(char[] buff, int count) {
        char[] temp = new char[count];
        for (int i = 0; i < count; i++) {
            temp[i] = buff[i];
        }
        return new String(temp);
    }


    public synchronized void sendMsg(String sendMsg) {
        if (serverRuning && mSocketServer.size() > 0) {
            String msgText = sendMsg;//取得编辑框中我们输入的内容
            if (msgText.length() > 0) {
                try {
                    for (int i = 0; i < mPrintWriterServer.size(); i++) {
                        mPrintWriterServer.get(i).print(msgText);//发送给客户端
                        mPrintWriterServer.get(i).flush();
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    //Toast.makeText(mContext, "发送异常：" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    public void close() {
        if (serverRuning) {
            serverRuning = false;
				/*
				try {
					if(serverSocket!=null)
					{
						serverSocket.close();
						serverSocket = null;
					}
					if(mSocketServer!=null)
					{
						mSocketServer.close();
						mSocketServer = null;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} */

            try {
                if (serverSocket != null) {

                    serverSocket.close();
                }

                if (mSocketServer.size() > 0) {
                    Socket s;
                    for (int i = 0; i < mSocketServer.size(); i++) {

                        s = mSocketServer.get(i);
                        s.close();
                        s = null;
                    }

                    mSocketServer.clear();
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            interruptThread();  //关闭tcp通信相关的线程;
        }
    }

    private void createServerThread() {
        mcreateServerRunnable temp = new mcreateServerRunnable();
        Thread t = new Thread(temp);
        mThreadServer.add(t);
        t.start();


    }

    private void interruptThread() {
        for (int i = 0; i < mThreadServer.size(); i++) {
            Thread t = mThreadServer.get(i);
            t.interrupt();
        }

    }

    public int ClientSize() {
        return mThreadServer.size() - 1;

    }

    public static class ClientVo{
        private String ip;
        private String name;

        public ClientVo() {
        }

        public ClientVo(String ip, String name) {
            this.ip = ip;
            this.name = name;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }



}