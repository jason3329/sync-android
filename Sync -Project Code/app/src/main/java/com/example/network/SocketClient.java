package com.example.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import com.example.gproject.ClientActivity;
import com.example.gproject.FileSender_ClientActivity;
import com.example.gproject.MyListener;
import com.jump.canvasdraw.CanvasDrawActivity;
import com.jump.canvasdraw.HandWrite;
import com.poi.poiandroid.DrawBegin;
import com.poi.poiandroid.PPTActivity;
import com.poi.word.ReadWordActivity;


import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;


public class SocketClient {


    private MyListener ClientListener;
    private DrawBegin drawbegin;
    private String SERVERIP;
    private static final int SERVERPORT = SocketServer.SERVERPORT;
    private Thread mThread = null;
    private Socket mSocket = null;
    private BufferedReader mBufferedReader = null;
    private PrintWriter mPrintWriter = null;
    private String recMsg = "";

    private String recvMessageClient;

    private boolean isConnecting = false;
    private Socket mSocketClient = null;
    static PrintWriter mPrintWriterClient = null;
    private Thread mThreadClient = null;
    static BufferedReader mBufferedReaderClient = null;

    private static SocketClient ClientInstance = null;  //����Ϊȫ�־�̬����;

    public SocketClient(MyListener m, String ip) {
        ClientListener = m;
        SERVERIP = ip;
        ClientInstance = SocketClient.this;
    }

    public static SocketClient getInstance() {
        if (ClientInstance != null)

        {
            return ClientInstance;
        }
        return null;
    }

    public void setDrawListner(DrawBegin t) {
        drawbegin = t;
    }






    //���ӷ�����;
    public void link() {
        if (isConnecting) {
            isConnecting = false;
            try {
                if (mSocketClient != null) {
                    mSocketClient.close();
                    mSocketClient = null;

                    mPrintWriterClient.close();
                    mPrintWriterClient = null;
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            mThreadClient.interrupt();


        } else {
            isConnecting = true;
            mThreadClient = new Thread(mRunnable);
            mThreadClient.start();
        }

    }

    public void sendMsg(String sendMsg) {
        if (isConnecting && mSocketClient != null) {
            String msgText = sendMsg;//ȡ�ñ༭�����������������
            if (msgText.length() <= 0) {

            } else {
                try {
                    mPrintWriterClient.print(msgText);//���͸������
                    mPrintWriterClient.flush();
                } catch (Exception e) {
                    // TODO: handle exception
                    //Toast.makeText(mContext, "�����쳣��" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            //Toast.makeText(mContext, "û������", Toast.LENGTH_SHORT).show();
        }
    }


    // ////////////////////////////////////////////////////////////////////////////////////
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            // ˢ��
            try {
                String message = recvMessageClient;

                if (message.equals("clearall")) {
                    CanvasDrawActivity.clearControl();
                }
                else if (message.equals("WritePower")){
                    HandWrite.Writeable = true;}
                else if (message.equals("WritePowerCancel")){
                    HandWrite.Writeable = false;}
                else if (message.equals("blackcolor")) {
                    CanvasDrawActivity.setblue();
                } else if (message.equals("greencolor")) {
                    CanvasDrawActivity.setgreen();
                } else if (message.equals("redcolor")) {
                    CanvasDrawActivity.setred();
                }
                else if(message.equals("Width1")){
                    CanvasDrawActivity.handWrite.strokeWidth =3.0f;
                }
                else if(message.equals("Width2")){
                    CanvasDrawActivity.handWrite.strokeWidth =6.0f;
                }
                else if(message.equals("Width3")){
                    CanvasDrawActivity.handWrite.strokeWidth =9.0f;
                }
                else if (!message.contains(":")) {
                    ClientListener.refreshActivity(recvMessageClient); //���ļ�;
                } else {
                    String s[] = message.split(":");

                    //����������ppt��ҳʱ���ͻ��˸���ҳ��;
                    if (s[0].equals("page")) {
                        int position = Integer.parseInt(s[s.length - 1]);
                        PPTActivity.PageChange(position);
                    }

                    //�����ļ���
                    if (s[0].equals("file")) {
                        ClientListener.pptOpen(s[1]);
                    }

                    //ͬ������;
                    if (s[0].equals("scroll")) {
                        int y = Integer.parseInt(s[s.length - 1]);
                        ReadWordActivity.ScrollTo(y);
                    }

                    //ͬ����ע;

                    if (s[0].equals("drawmove")) {
                        String s2 = s[s.length - 1];
                        CanvasDrawActivity.drawControl(s2);

                    }

                    if (s[0].equals("drawbegin")) {
                        drawbegin.beginDraw();

                    }

                }


                //Log.i("���������",message );
            } catch (Exception e) {
                Log.e("�������", e.toString());
            }
        }
    };
    // ////////////////////////////////////////////////////////////////////////////////////
    

    //�߳�:�����������������Ϣ
    private Runnable mRunnable = new Runnable() {
        public void run() {

            String sIP = SERVERIP;
            int port = SERVERPORT;


            try {
                //连接服务器
                mSocketClient = new Socket(sIP, port);    //portnum
                //取得输入输出流
                mBufferedReaderClient = new BufferedReader(new InputStreamReader(mSocketClient.getInputStream()));

                mPrintWriterClient = new PrintWriter(mSocketClient.getOutputStream(), true);

                //recvMessageClient = "�Ѿ�����server!\n";//��Ϣ����
                Message msg = new Message();
                msg.what = 1;
                mHandler.sendMessage(msg);
                //break;
            } catch (Exception e) {
                //recvMessageClient = "����IP�쳣:" + e.toString() + e.getMessage() + "\n";//��Ϣ����
                Message msg = new Message();
                msg.what = 1;
                mHandler.sendMessage(msg);
                return;
            }

            char[] buffer = new char[25600];
            int count = 0;
            while (isConnecting) {
                try {
                    //if ( (recvMessageClient = mBufferedReaderClient.readLine()) != null )
                    if ((count = mBufferedReaderClient.read(buffer)) > 0) {
                        recvMessageClient = getInfoBuff(buffer, count);//��Ϣ����

                        System.out.println("jieshouzhong" + recvMessageClient);
                        Message msg = new Message();
                        msg.what = 1;
                        mHandler.sendMessage(msg);
                    }
                } catch (Exception e) {
                    //recvMessageClient = "�����쳣:" + e.getMessage() + "\n";//��Ϣ����
                    Message msg = new Message();
                    msg.what = 1;
                    mHandler.sendMessage(msg);
                }
            }
        }
    };

    private String getInfoBuff(char[] buff, int count) {
        char[] temp = new char[count];
        for (int i = 0; i < count; i++) {
            temp[i] = buff[i];
        }
        return new String(temp);
    }


    public void close() {
        if (isConnecting) {
            isConnecting = false;
            try {
                if (mSocketClient != null) {
                    mSocketClient.close();
                    mSocketClient = null;

                    mPrintWriterClient.close();
                    mPrintWriterClient = null;
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            mThreadClient.interrupt();
        }
    }


}