package com.example.network;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.example.network.SocketServer.mcreateServerRunnable;
import com.poi.poiandroid.PPTActivity;

import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * TCP发送文件线程
 */
public class NetTcpFileSendThread {
    private final static String TAG = "NetTcpFileSendThread";
    private String[] filePathArray;    //保存发送文件路径的数组

    private static int num = 0;

    public static ServerSocket server;
    private Socket socket;
    private byte[] readBuffer = new byte[1024];

    private List<Thread> mFileThreadServer;

    private ProgressDialog progress_Dialog;

    private int sendNum;   //成功发送的线程数

    private int ClientSize;

    private File sendFile;    //要发送的文件

//    private boolean FileType;



    public NetTcpFileSendThread(String[] filePathArray, ProgressDialog m_Dialog) {
        this.filePathArray = filePathArray;
        progress_Dialog = m_Dialog;
        sendNum = 0;


        mFileThreadServer = new ArrayList<Thread>();

        try {
            server = new ServerSocket(8777+ num);
            createFileServerThread();  //创建新线程


        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.e(TAG, "监听TCP端失败");
        }
    }

    public NetTcpFileSendThread(String[] filePathArray, ProgressDialog m_Dialog, int clientsize) {
        this.filePathArray = filePathArray;
        progress_Dialog = m_Dialog;
        sendNum = 0;
        ClientSize = clientsize;
//        FileType = filetype;


        mFileThreadServer = new ArrayList<Thread>();
        sendFile = new File(filePathArray[0]);    //要发送的文件


        try {
            /*
			server = new ServerSocket(8777);
			for(int i=0;i<ClientSize;i++)
			{ createFileServerThread();  }//创建新线程	*/


            server = new ServerSocket(8777+num);
            Log.i("num:",""+num);
            num++;
            createFileServerThread();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Log.e(TAG, "监听tcp端口失败");
        }
    }

    // ////////////////////////////////////////////////////////////////////////////////////
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            //刷新
            try {
                if (msg.what == 1)    //发送文件成功后
                {
                    sendNum++;

                    if (sendNum == ClientSize) {
                        close();
                        //Thread.sleep(1500);
                        progress_Dialog.dismiss();
                    }


                }

            } catch (Exception e) {
                Log.e("监听出错", e.toString());
            }
        }
    };
    // ////////////////////////////////////////////////////////////////////////////////////


    class mcreateFileServerRunnable implements Runnable {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            for (int i = 0; i < filePathArray.length; i++) {
                try {
                    socket = server.accept();
                    Log.i(TAG, "与IP为" + socket.getInetAddress().getHostAddress() + "的用户建立tcp连接");

                    //成功连接后再创建一个新的线程
                    createFileServerThread();

                    //获取socket类的缓冲区输入输出流
                    BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
                    BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
                    int mlen = bis.read(readBuffer);
                    String ipmsgStr = new String(readBuffer, 0, mlen, "gbk");


                    Log.d(TAG, "收到的tcp数据信息内容是：" + ipmsgStr);

                    //IpMessageProtocol ipmsgPro = new IpMessageProtocol(ipmsgStr);
                    //String fileNoStr = null ;
                    //String[] fileNoArray = fileNoStr.split(":");
                    //int sendFileNo = Integer.valueOf(fileNoArray[1]);


                    Log.d(TAG, "本次发送的文件具体路径为" + filePathArray[0]);

                    //File sendFile ;	//要发送的文件
                    BufferedInputStream fbis;

                    int rlen = 0;

                    synchronized (sendFile) {

                        fbis = new BufferedInputStream(new FileInputStream(sendFile));

                        while ((rlen = fbis.read(readBuffer)) != -1) {
                            bos.write(readBuffer, 0, rlen);

                        }
                        bos.flush();
                    }
                    Log.i(TAG, "文件发送成功");

                    Message msg = new Message();
                    msg.what = 1;
                    mHandler.sendMessage(msg);

                    if (bis != null) {
                        bis.close();
                        bis = null;
                    }

                    if (fbis != null) {
                        fbis.close();
                        fbis = null;
                    }

                    if (bos != null) {
                        bos.close();
                        bos = null;
                    }

                    if (i == (filePathArray.length - 1)) {
                        //文件发送成功
                    }

                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Log.e(TAG, "接受数据时，系统不支持GBK编码");
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Log.e(TAG, "发生IO错误");
                    break;
                }


            }


        }

    }


    private void createFileServerThread() {
        mcreateFileServerRunnable temp = new mcreateFileServerRunnable();
        Thread t = new Thread(temp);
        mFileThreadServer.add(t);
        t.start();

    }

    public void close() {
        Log.d(TAG, "进入close");
        try {
            if (socket != null) {

                socket.close();
                socket = null;
            }


            if (server != null) {

                server.close();
                server = null;
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        interruptThread();  //关闭tcp通信相关线程
    }

    private void interruptThread() {
        for (int i = 0; i < mFileThreadServer.size(); i++) {
            Thread t = mFileThreadServer.get(i);
            t.interrupt();
        }

        mFileThreadServer.clear();

    }

}
