package com.example.network;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;

import android.app.ProgressDialog;
import android.os.Message;
import android.util.Log;


/**
 * TCP接受文件线程类
 */
public class NetTcpFileReceiveThread implements Runnable {
    private final static String TAG = "NetTcpFileReceiveThread";

    private String[] fileInfos;    //文字信息字符组
    private String senderIp;
    private long packetNo;    //包编号
    public static String savePath = "/mnt/sdcard/FeigeRec/";; //文件保存路径


    private String selfGroup;

    private Socket socket;
    private BufferedInputStream bis;
    private BufferedOutputStream bos;
    BufferedOutputStream fbos;
    private byte[] readBuffer = new byte[512];

    private String filename;

    private ProgressDialog progress_Dialog;  //接受文件进度框

    public NetTcpFileReceiveThread(String packetNo, String senderIp, String[] fileInfos, String FileName) {
        this.packetNo = Long.valueOf(packetNo);
        this.fileInfos = fileInfos;
        this.senderIp = senderIp;

        selfGroup = "android";
        savePath = "/mnt/sdcard/FeigeRec/";

        filename = FileName;

        //判断接受文件的文件夹是否存在，若不存在，则创建
        File fileDir = new File(savePath);
        if (!fileDir.exists()) {    //若不存在
            fileDir.mkdir();
        }

    }

    public NetTcpFileReceiveThread(String packetNo, String senderIp, String[] fileInfos, String FileName, ProgressDialog m_Dialog) {
        this.packetNo = Long.valueOf(packetNo);
        this.fileInfos = fileInfos;
        this.senderIp = senderIp;

        this.progress_Dialog = m_Dialog;

        selfGroup = "android";

//        if(filetype)      //when using filesender filetype's value = false;
//        {
//            savePath="/mnt/sdcard/Apple/";
//            Log.i("is","apple");
//        }
//        else {

//            Log.i("is","feige");
//        }

        filename = FileName;

        //判断接受文件的文件夹是否存在，若不存在，则创建
        File fileDir = new File(savePath);
        if (!fileDir.exists()) {    //若不存在
            fileDir.mkdir();
        }

    }

    public static void FilerSender_ChangePath (String path){
        savePath = path;
        return ;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        for (int i = 0; i < fileInfos.length; i++) {    //循环接受每个文件

            try {
                socket = new Socket(senderIp, 8777);
                Log.d(TAG, "�������Ϸ��Ͷ�");
                bos = new BufferedOutputStream(socket.getOutputStream());

                //发送收取文件飞鸽命令
                byte[] sendBytes = "link".getBytes();
                bos.write(sendBytes, 0, sendBytes.length);
                bos.flush();


                //接受文件
                File receiveFile = new File(savePath + filename);
                if (receiveFile.exists()) {    //若对应文件名的文件已存在，则删除原来的文件
                    receiveFile.delete();
                }

                fbos = new BufferedOutputStream(new FileOutputStream(receiveFile));
                Log.d(TAG, "准备开始接受文件...");
                bis = new BufferedInputStream(socket.getInputStream());
                int len = 0;
                long sended = 0;    //已接受文件大小
                int temp = 0;
                while ((len = bis.read(readBuffer)) != -1) {
                    fbos.write(readBuffer, 0, len);
                    fbos.flush();

                }

                //Log.i(TAG, "第" + (i+1) + "个文件接受成功，文件名为"  + fileInfo[1]);
                int[] success = {i + 1, fileInfos.length};
                Message msg4success = new Message();

                msg4success.obj = success;


                progress_Dialog.dismiss();


            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.e(TAG, "系统不支持GBK编码");
            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.e(TAG, "远程IP地址错误");
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.e(TAG, "文件创建失败");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Log.e(TAG, "发送IO错误");
            } finally {    //处理

                if (bos != null) {
                    try {
                        bos.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    bos = null;
                }

                if (fbos != null) {
                    try {
                        fbos.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    fbos = null;
                }

                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    bis = null;
                }

                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    socket = null;
                }

            }


        }

    }

}
