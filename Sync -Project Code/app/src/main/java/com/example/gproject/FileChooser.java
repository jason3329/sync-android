package com.example.gproject;

import java.io.File;  
import java.util.ArrayList;  
import java.util.Collections;
import java.util.HashMap;
import java.util.List;  
import java.util.Map;

import com.poi.poiandroid.R;
  
import android.app.Dialog;  
import android.content.Context;  
import android.os.Bundle;  
import android.os.Environment;  
import android.os.Handler;  
import android.view.Gravity;  
import android.view.LayoutInflater;
import android.view.View;  
import android.view.ViewGroup;
import android.widget.AdapterView;  
import android.widget.AdapterView.OnItemClickListener;  
import android.widget.ArrayAdapter;  
import android.widget.BaseAdapter;
import android.widget.Button;  
import android.widget.EditText;  
import android.widget.ImageView;
import android.widget.LinearLayout;  
import android.widget.ListView;  
import android.widget.SimpleAdapter;
import android.widget.TextView;  
import android.widget.Toast;  
  
/** 
 * 
 */  
public class FileChooser extends Dialog implements android.view.View.OnClickListener{  
      
	List<HashMap<String, Object>> mHashMaps =new   ArrayList<HashMap<String, Object>>() ;
	
    private ListView list;  
    MyAdapter Adapter;  
      
    Context context;  
    private String path;  
    
    private MyListener pathBack;
      
    private TextView title;  
    private Button home,back,ok;  
    private LinearLayout titleView;  
      
    private int type = 1;  
    private String[] fileType = null;  
      
    public final static int TypeOpen = 1;  

      
    /** 
     * @param context 
     * @param type 值为1表示创建打开目录类型的对话框
     * @param fileType 要过滤的文件类型,null表示只选择目录 
     * @param resultPath 点OK按钮返回的结果，目录或者目录+文件名 
     */  
    public FileChooser(Context context,int type,String[]fileType,String resultPath,MyListener myListener) {  
        super(context);  
        // TODO Auto-generated constructor stub  
        this.context = context;  
        this.type = type;  
        this.fileType = fileType;  
        this.path = resultPath;  
        this.pathBack=myListener;
    }

    /* (non-Javadoc) 
     * @see android.app.Dialog#dismiss() 
     */  
    @Override  
    public void dismiss() {  
        // TODO Auto-generated method stub  
        super.dismiss();  
    }  
    /* (non-Javadoc) 
     * @see android.app.Dialog#onCreate(android.os.Bundle) 
     */  
    @Override  
    protected void onCreate(Bundle savedInstanceState) {  
        // TODO Auto-generated method stub  
        super.onCreate(savedInstanceState);  
        setContentView(R.layout.chooserdialog);  
          
        path = getRootDir();  
        //arr = (ArrayList<String>) getDirs(path);
        mHashMaps = getDirsMap(path);
        Adapter =new MyAdapter(context);
        
          
        list = (ListView)findViewById(R.id.list_dir);  
        list.setAdapter(Adapter);  
          
        list.setOnItemClickListener(lvLis);  
  
        home = (Button) findViewById(R.id.btn_home);  
        home.setOnClickListener(this);  
          
        back = (Button) findViewById(R.id.btn_back);  
        back.setOnClickListener(this);  
          
        ok = (Button) findViewById(R.id.btn_ok);  
        ok.setOnClickListener(this);  
          
        titleView = (LinearLayout) findViewById(R.id.dir_layout);  
          
        if(type == TypeOpen){  
            title = new TextView(context);  
            titleView.addView(title);  
            title.setText(path);  
        }
//      title = (TextView) findViewById(R.id.dir_str);  
//      title.setText(path);  
          
    }  
    //动态更新ListView  
    Runnable add=new Runnable(){  
  
        @Override  
        public void run() {  
            // TODO Auto-generated method stub  
            //arr.clear();  
            mHashMaps.clear();
  
            //必须得用这种方法为arr赋值才能更新  

                       
            mHashMaps = getDirsMap(path);
           
                     
            Adapter.notifyDataSetChanged();  
        }         
    };  
     
    private OnItemClickListener lvLis=new OnItemClickListener(){  
        @Override  
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,  
                long arg3) {  
            String temp = (String) mHashMaps.get(arg2).get("FILE");  
            
            if(temp.equals(".."))  
                path = getSubDir(path);  
            else if(path.equals("/"))  
                path = path+temp;  
            else  
                path = path+"/"+temp;  
              
//System.out.println("OnItemClick path2"+path);   
            if(type == TypeOpen)  
                title.setText(path);  
              
            Handler handler=new Handler();  
            handler.post(add);  
        }  
    };  
       
    
    private List<HashMap<String, Object>>  getDirsMap(List<String> myFile){  

    	List<HashMap<String, Object>> mMaps =new   ArrayList<HashMap<String, Object>>() ;
    	
        for(int i = 0; i < myFile.size(); i++)  
        {  
			HashMap<String, Object> temp_map = new HashMap<String, Object>();
			temp_map.put("PIC", R.drawable.file_pic);
			temp_map.put("FILE", myFile.get(i));
        
			mMaps.add(temp_map);
        } 
    	
        return mMaps;  
    } 
    
    private List<HashMap<String, Object>>  getDirsMap(String ipath){  

    	List<HashMap<String, Object>> mMaps =new   ArrayList<HashMap<String, Object>>() ;
    	
        List<String> dir = new ArrayList<String>(); //存储文件夹名;
        List<String> file = new ArrayList<String>();  //存储文件名;
        
//System.out.println("GetDirs path:"+ipath);          
        File[] myFile = new File(ipath).listFiles();  
        if(myFile == null){  
            dir.add("..");
  			HashMap<String, Object> temp_map = new HashMap<String, Object>();
   			temp_map.put("PIC", R.drawable.file_pic);
   			temp_map.put("FILE","..");
   			mMaps.add(temp_map);
              
        }else
        {
            for(File f: myFile){  
                //过滤目录  
                if(f.isDirectory()){  
                    String tempf = f.toString();  
                    int pos = tempf.lastIndexOf("/");  
                    String subTemp = tempf.substring(pos+1, tempf.length());  
//                  String subTemp = tempf.substring(path.length(),tempf.length());  
                    dir.add(subTemp);    
                    
                }
                //过滤知道类型的文件  
                if(f.isFile() && fileType != null){
                    for(int i = 0;i< fileType.length;i++){
                        int typeStrLen = fileType[i].length();

                        String fileName = f.getPath().substring(f.getPath().length()- typeStrLen);
                        file.add(f.toString().substring(path.length()+1,f.toString().length()));
//                        if (fileName.toLowerCase().equals(fileType[i])) {
//                            file.add(f.toString().substring(path.length()+1,f.toString().length()));
//                        }
                    }
                }
            }
            
            Collections.sort(dir,String.CASE_INSENSITIVE_ORDER);  //对文件夹名排序;           
            Collections.sort(file,String.CASE_INSENSITIVE_ORDER);  //对文件名排序;
            
           for(int i = 0; i < dir.size(); i++)  
            {  
   			HashMap<String, Object> temp_map = new HashMap<String, Object>();
   			temp_map.put("PIC", R.drawable.file_pic);
   			temp_map.put("FILE", dir.get(i));
            
   			mMaps.add(temp_map);
            }  
            
           for(int i = 0; i < file.size(); i++)  
           {  
      			HashMap<String, Object> temp_map = new HashMap<String, Object>();
       			temp_map.put("PIC", R.drawable.office_file);
       			temp_map.put("FILE", file.get(i));
       			mMaps.add(temp_map);
           }  
            
        }
       
        
 
        return mMaps;  
    } 
    
    
 
    @Override  
    public void onClick(View v) {  
        // TODO Auto-generated method stub  
        if(v.getId() == home.getId()){  
            path = getRootDir();  
            if(type == TypeOpen)  
                title.setText(path);              
            Handler handler=new Handler();  
            handler.post(add);  
        }else if(v.getId() == back.getId()){  
            path = getSubDir(path);  
            if(type == TypeOpen)  
                title.setText(path);              
            Handler handler=new Handler();  
            handler.post(add);  
        }else if(v.getId() == ok.getId()){  
            dismiss();  

            Toast.makeText(context, path, Toast.LENGTH_SHORT).show();  
            pathBack.refreshActivity(path);
            //pathBack.pptOpen();
            
            
        }  
              
          
    }  
      
    private String getSDPath(){   
           File sdDir = null;   
           boolean sdCardExist = Environment.getExternalStorageState()     
                               .equals(android.os.Environment.MEDIA_MOUNTED);   //判断sd卡是否存在   
           if(sdCardExist)     
           {                                 
             sdDir = Environment.getExternalStorageDirectory();//获取根目录   
          }     
           if(sdDir == null){  
//Toast.makeText(context, "No SDCard inside!",Toast.LENGTH_SHORT).show();  
               return null;  
           }  
           return sdDir.toString();   
             
    }   
      
    private String getRootDir(){  
        String root = "/sdcard";  
          
        path = getSDPath();  
        if (path == null)  
            path="/sdcard";  
          
        return root;  
    }  
      
    private String getSubDir(String path){  
        String subpath = null;  
          
        int pos = path.lastIndexOf("/");  
          
        if(pos == path.length()){  
            path = path.substring(0,path.length()-1);  
            pos = path.lastIndexOf("/");  
        }  
          
        subpath = path.substring(0,pos);  
          
        if(pos == 0)  
            subpath = path;  
          
        return subpath;  
    } 
    

        
        //ViewHolder静态类
        static class ViewHolder
        {
            public ImageView pic;
            public TextView file;
        }
        
        public class MyAdapter extends BaseAdapter
        {    
            private LayoutInflater mInflater = null;
            
            private MyAdapter(Context context)
            {
                //根据context上下文加载布局，这里的是Demo17Activity本身，即this
                this.mInflater = LayoutInflater.from(context);
            }

            @Override
            public int getCount() {
                //How many items are in the data set represented by this Adapter.
                //在此适配器中所代表的数据集中的条目数
                return mHashMaps.size();
            }

            @Override
            public Object getItem(int position) {
                // Get the data item associated with the specified position in the data set.
                //获取数据集中与指定索引对应的数据项
                return position;
            }

            @Override
            public long getItemId(int position) {
                //Get the row id associated with the specified position in the list.
                //获取在列表中与指定索引对应的行id
                return position;
            }
            
            //Get a View that displays the data at the specified position in the data set.
            //获取一个在数据集中指定索引的视图来显示数据
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                ViewHolder holder = null;
                //如果缓存convertView为空，则需要创建View
                if(convertView == null)
                {
                    holder = new ViewHolder();
                    //根据自定义的Item布局加载布局
                    convertView = mInflater.inflate(R.layout.fileitem, null);
                    holder.file= (TextView)convertView.findViewById(R.id.textView);
                    holder.pic = (ImageView)convertView.findViewById(R.id.fileView);

                    //将设置好的布局保存到缓存中，并将其设置在Tag里，以便后面方便取出Tag
                    convertView.setTag(holder);
                }else
                {
                    holder = (ViewHolder)convertView.getTag();
                }
                holder.pic.setBackgroundResource((Integer)mHashMaps.get(position).get("PIC"));
                holder.file.setText((String)mHashMaps.get(position).get("FILE"));
                
                return convertView;
            }
            
        }


    
}  



