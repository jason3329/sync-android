package com.example.gproject;


import java.io.ByteArrayOutputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import com.example.network.SocketServer;
import com.example.network.WifiAdmin;
import com.example.network.NetTcpFileSendThread;
import com.jump.canvasdraw.CanvasDrawActivity;
import com.jump.canvasdraw.HandWrite;
import com.poi.poiandroid.PPTActivity;
import com.poi.poiandroid.R;
import com.poi.word.ReadWordActivity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ServerActivity extends Activity {

    private Button SyncshowButton;//同步演示器按钮
    private Button fileButton; //选择文件按钮;
    private Button sendButton; //发送文件按钮;
    private Button playButton; //播放文件按钮;
    private Button backButton; //back
    private Button BoardButtion;//白板按钮
    private Button fileSentButtion;//文件发送器

    public static List<String> Iplist = new ArrayList<>();

    public static TextView IpText;
    public static TextView nickname;
    private WifiAdmin wifi;
    public static SocketServer server = null;



    //private NetTcpFileSendThread SendFileThread =null ;

    //利用TextView来将选择的文件路径传到activity中；
    final MyListener myListener = new MyListener() {

        public void refreshActivity(String text) {

            TextView pathView = (TextView) findViewById(R.id.pathView);
            pathView.setText(text);

        }


        @Override
        public void pptOpen(String fm) {
            // TODO Auto-generated method stub
//            serverMap.put    ("192.168.1.110", server);
//			serverMap.get("192.168.1.110");
        }


    };


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server);

        SyncshowButton = (Button) findViewById(R.id.Syncshow_button);
        BoardButtion = (Button) findViewById(R.id.WhiteBoard_button);
        fileSentButtion = (Button) findViewById(R.id.FileSent_button);

        IpText = (TextView) findViewById(R.id.IP_Address);
        nickname = (TextView)findViewById(R.id.nickname);
        wifi = new WifiAdmin(ServerActivity.this);

        buildServer();


//

        SyncshowButton.setOnClickListener(
                new OnClickListener() {
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        setContentView(R.layout.showserver);

                        fileButton = (Button) findViewById(R.id.ChooseFile_button);
                        sendButton = (Button) findViewById(R.id.SendFile_button);
                        playButton = (Button) findViewById(R.id.PlayFile_button);
                        backButton = (Button) findViewById(R.id.back_button);


                        //选择文件按钮功能;
                        fileButton.setOnClickListener(
                                new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        String path = null;
                                        String[] fileType = {"ppt", "doc"};//要过滤的文件类型列表
                                        FileChooser dlg = new FileChooser(ServerActivity.this, 1, fileType, path, myListener);
                                        dlg.setTitle("Choose dst file dir");
                                        dlg.show();
                                    }
                                }
                        );

                        //传送文件按钮功能；
                        sendButton.setOnClickListener(
                                new OnClickListener() {
                                    public void onClick(View v) {
                                        // TODO Auto-generated method stub


                                        ProgressDialog m_Dialog = ProgressDialog.show(ServerActivity.this, "请等待...", "正在传输文件，请稍后...", true);
                                        String filename = getFileName(getFilePath());
                                        SendFileLink(m_Dialog);
                                        server.sendMsg("file:" + filename);

                                    }
                                }
                        );


                        //开始演示按钮功能
                        playButton.setOnClickListener(
                                new OnClickListener() {
                                    public void onClick(View v) {
                                        // TODO Auto-generated method stub
                                        if (server != null) {
                                            String s = getFilePath();
                                            String a[] = s.split("/");
                                            server.sendMsg(a[a.length - 1]);
                                        }

                                        pptOpen();
                                    }
                                }
                        );


                        backButton.setOnClickListener(
                                new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        onCreate(savedInstanceState);
                                    }
                                }
                        );


                    }

//            public boolean dispatchKeyEvent(KeyEvent event)
//            {
//                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK )
//                {
//                    setContentView(R.layout.activity_server) ;
//                }
//                return super.dispatchKeyEvent(event);
//
//            }
                }
        );


        BoardButtion.setOnClickListener(
                new OnClickListener() {
                    public void onClick(View view) {
                        Drawable drawable = getResources().getDrawable(R.drawable.white);
                        Bitmap b = drawableToBitmap(drawable);
                        Intent intent = new Intent(ServerActivity.this, CanvasDrawActivity.class);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        b.compress(Bitmap.CompressFormat.PNG, 100, baos);
                        byte[] bitmapByte = baos.toByteArray();
                        server.sendMsg("whiteboard");//向客户端发送“打开电子白板”的控制信息
                        intent.putExtra("type", "server");
                        intent.putExtra("bitmap", bitmapByte);
                        HandWrite.Writeable = true;
                        startActivity(intent);
                    }
                }
        );

        fileSentButtion.setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ServerActivity.this, FileSenderActivity.class);
                        server.sendMsg("commonfile");
                        startActivity(intent);
                    }
                }
        );

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public static void showname(int a,String name){
        String i = SocketServer.Nickname.get(a);
        nickname.append(name+"、");
        return;
    }

    //通过读取TextView来读取选中文件路径;
    public String getFilePath() {
        TextView pathView = (TextView) findViewById(R.id.pathView);

        String fpath = (String) pathView.getText();
        Log.d("路径", fpath);
        return fpath;

    }


    //创建服务器;
    private void buildServer() {
        if (server == null) {
            server = new SocketServer(myListener);
            server.createServer();

            Toast.makeText(getApplicationContext(), "成功创建服务器", Toast.LENGTH_SHORT).show();


            String s = "";
            try {
                s = wifi.getLocalIP4Address();
            } catch (SocketException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            IpText.append(s);
        }
    }

    //创建线程用于发送文件；
    private void SendFileLink(ProgressDialog m_Dialog) {
        String pathArray[] = new String[1];
        int clientnum = server.ClientSize();
        pathArray[0] = "/mnt" + getFilePath();

        //if(SendFileThread != null)  { SendFileThread.close(); SendFileThread=null; }
        NetTcpFileSendThread SendFileThread = new NetTcpFileSendThread(pathArray, m_Dialog, clientnum);
        //netTcpFileSendThread.start();	//启动线程
    }

    private void pptOpen() {
        //跳转到ppt播放的activity;
        TextView pathView = (TextView) findViewById(R.id.pathView);
        String s = (String) pathView.getText();
        String filename = getFileName(s);

        String filetype = filename.substring(filename.indexOf(".") + 1);

        if (filetype.equals("ppt")) {
            Intent intent = new Intent(ServerActivity.this, PPTActivity.class);
            Uri uri = Uri.parse((String) pathView.getText());
            intent.setData(uri);
            intent.putExtra("type", "server");
            startActivity(intent);
        }

        if (filetype.equals("doc")) {
            Intent intent = new Intent(ServerActivity.this, ReadWordActivity.class);
            Uri uri = Uri.parse("/mnt" + (String) pathView.getText());
            intent.setData(uri);
            intent.putExtra("type", "server");
            intent.putExtra("filename", filename);

            startActivity(intent);
        }


    }

    public String getFileName(String pathandname) {

        int start = pathandname.lastIndexOf("/");
        if (start != -1) {
            return pathandname.substring(start + 1);
        } else {
            return null;
        }

    }

    public void onDestroy() {
        super.onDestroy();

        if (server != null) {
            server.close();
            server = null;
        }

        //if(SendFileThread != null)  { SendFileThread.close(); SendFileThread=null; }


    }

    private Bitmap drawableToBitmap(Drawable drawable) {
        int w = drawable.getIntrinsicWidth();
        int h = drawable.getIntrinsicHeight();
        Bitmap.Config config =
                drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
                        : Bitmap.Config.RGB_565;
        Bitmap bitmap = Bitmap.createBitmap(w, h, config);
        //注意，下面三行代码要用到，否在在View或者surfaceview里的canvas.drawBitmap会看不到图
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, w, h);
        drawable.draw(canvas);
        return bitmap;
    }

}






