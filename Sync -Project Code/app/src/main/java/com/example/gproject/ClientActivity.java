package com.example.gproject;

import com.example.network.SocketClient;
import com.example.network.NetTcpFileReceiveThread;

import com.example.network.WifiAdmin;
import com.jump.canvasdraw.CanvasDrawActivity;
import com.poi.poiandroid.PPTActivity;
import com.poi.poiandroid.R;
import com.poi.word.ReadWordActivity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.net.SocketException;
import java.util.logging.Handler;

public class ClientActivity extends Activity {

    private Button LinkButton;
    private EditText IpAd;
    private SocketClient client = null;
    private TextView IPClient = null;
    private WifiAdmin wifi;
    private String clientip = null;


    private String filename;



    public final MyListener myListener = new MyListener() {

        //打开所选文件
        public void refreshActivity(String text) {
            String message = text;

            if (!text.contains("Bundle")) {
             /*
			 Intent intent = new Intent(ClientActivity.this,PPTActivity.class);  
			  Uri uri = Uri.parse("/sdcard/FeigeRec/"+message); 
			 intent.setData(uri);
			 intent.putExtra("type", "client");
			 
			 startActivity(intent); */


                String filetype = message.substring(message.indexOf(".") + 1);

                if (filetype.equals("ppt")) {
                    Intent intent = new Intent(ClientActivity.this, PPTActivity.class);
                    Uri uri = Uri.parse("/sdcard/FeigeRec/" + message);
                    intent.setData(uri);
                    intent.putExtra("type", "client");
                    startActivity(intent);
                    return;
                }

                if (filetype.equals("doc")) {
                    Intent intent = new Intent(ClientActivity.this, ReadWordActivity.class);
                    Uri uri = Uri.parse("/mnt/sdcard/FeigeRec/" + message);
                    intent.setData(uri);
                    intent.putExtra("type", "client");
                    intent.putExtra("filename", message);

                    startActivity(intent);
                    return;

                }

                if (text.equals("whiteboard")) {
                    Drawable drawable = getResources().getDrawable(R.drawable.white);
                    Bitmap b = drawableToBitamp(drawable);
//                        Bitmap b =((BitmapDrawable) drawable).getBitmap();
//                        Bitmap b = Bitmap.createBitmap(400,200, Bitmap.Config.ARGB_8888);

//                        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.white);
                    Intent intent = new Intent(ClientActivity.this, CanvasDrawActivity.class);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    b.compress(Bitmap.CompressFormat.PNG, 100, baos);
                    byte[] bitmapByte = baos.toByteArray();

                    intent.putExtra("type", "client");
                    intent.putExtra("bitmap", bitmapByte);
                    startActivity(intent);
                    return;
                }
                if (text.contains("commonfile")){
                    Intent intent = new Intent(ClientActivity.this, FileSender_ClientActivity.class);
                    Uri uri = Uri.parse("/mnt/sdcard/Apple/" + message);
//                  intent.putExtra("type", "client");
//                  intent.putExtra("filename",message);
                    startActivity(intent);
                }

            }

        }

        @Override
        public void pptOpen(String fm) {
            // TODO Auto-generated method stub
            filename = fm;
            showDialog(ClientActivity.this);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        IpAd = (EditText) findViewById(R.id.IpEditText);
        LinkButton = (Button) findViewById(R.id.LinkButton);
        IPClient = (TextView) findViewById(R.id.IPClient_address);
        wifi = new WifiAdmin(ClientActivity.this);


        try {
            clientip = wifi.getLocalIP4Address();
        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        IPClient.append(clientip);


        //客户端连接服务器
        LinkButton.setOnClickListener(
                new OnClickListener() {
                    public void onClick(View v) {

                        String ip = (String) IpAd.getText().toString();


                        if (client == null) {
                            client = new SocketClient(myListener, ip);
                            client.link();
                        }
                        client.sendMsg("link");
                        IpAd.setEnabled(false);
                        android.os.Handler handler = new android.os.Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                client.sendMsg("IP:" + clientip+"@"+"Nickname:"+FirstPageActivity.nickname);
//                                int newport = (int)Math.random()*90000+10000;
//                                client.sendMsg("Newport:"+newport);
                            }
                        }, 1000);

                        Toast.makeText(getApplicationContext(), "正在连接", Toast.LENGTH_SHORT).show();
                        Toast.makeText(getApplicationContext(), "连接成功", Toast.LENGTH_SHORT).show();




                    }
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.client, menu);
        return true;
    }

    //创建线程用于接收文件
    private void RecFileLink(ProgressDialog m_Dialog) {
        String pathArray[] = new String[1];

        String ip = (String) IpAd.getText().toString();
        Thread fileReceiveThread = new Thread(new NetTcpFileReceiveThread("0", ip, pathArray, filename, m_Dialog));    //�½�һ�������ļ��߳�
        fileReceiveThread.start();    //启动线程


    }


    private void showDialog(Context context) {

        //提示
		   /*
	    	AlertDialog.Builder builder = new AlertDialog.Builder(context);
	        builder.setTitle("提示");
	        builder.setMessage("是否接受文件");
	        builder.setPositiveButton("是", new android.content.DialogInterface.OnClickListener(){

				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					RecFileLink();
				}
	        	
	        });
	        
	        
	        builder.setNegativeButton("否", new android.content.DialogInterface.OnClickListener(){

				public void onClick(DialogInterface dialog, int which) {
					
				}
	        	
	        });
	        
	        builder.show();
	        
	        */

        ProgressDialog m_Dialog = ProgressDialog.show(ClientActivity.this, "请等待...", "正在接受文件，请稍后", true);

        RecFileLink(m_Dialog);


    }


    public void onDestroy() {
        super.onDestroy();

        if (client != null) {
            client.close();
        }

    }

    private Bitmap drawableToBitamp(Drawable drawable) {
        int w = drawable.getIntrinsicWidth();
        int h = drawable.getIntrinsicHeight();
        Bitmap.Config config =
                drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
                        : Bitmap.Config.RGB_565;
        Bitmap bitmap = Bitmap.createBitmap(w, h, config);
        //注意，下面三行代码要用到，否在在View或者surfaceview里的canvas.drawBitmap会看不到图
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, w, h);
        drawable.draw(canvas);
        return bitmap;
    }

    public void FileSenderJump(){


    }
}


