package com.example.gproject;


import com.poi.poiandroid.R;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.Toast;

public class FirstPageActivity extends Activity {

	private Button Settinginformation;
	private Button Beginconnect;
    private RadioGroup choosecharacter;
    private RadioButton isserver;
    private RadioButton isclient;
    private EditText name;
    public int choose = 2; //true:0 false:1
    public static String nickname;


	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.beginnning_page);
		
		
		Settinginformation =(Button) findViewById(R.id.settinginformation);
		Beginconnect =(Button) findViewById(R.id.beginconnect);

		
		
		Settinginformation.setOnClickListener(
		new OnClickListener(){
			public void onClick(View v){
	            // TODO Auto-generated method stub

                final AlertDialog.Builder builder= new AlertDialog.Builder(FirstPageActivity.this);
                builder.setIcon(R.drawable.office_file);
                builder.setTitle("设置信息");
                TableLayout setttingForm = (TableLayout)getLayoutInflater().inflate(R.layout.information_setting,null);
                builder.setView(setttingForm);



                choosecharacter = (RadioGroup)setttingForm.findViewById(R.id.choosecharacter);
                isserver = (RadioButton)setttingForm.findViewById(R.id.isserver);
                isclient = (RadioButton)setttingForm.findViewById(R.id.isclient);
                name = (EditText)setttingForm.findViewById(R.id.nickname);



                choosecharacter.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if (checkedId == isserver.getId())
                        {
                            choose = 0;
                        }
                        else if (checkedId == isclient.getId())
                        {
                            choose = 1;
                        }
                    }
                });

                builder.setPositiveButton("确定",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {



                        nickname = (String) name.getText().toString();
                        onCreate(savedInstanceState);
                    }
                });

                builder.setNegativeButton("取消",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onCreate(savedInstanceState);
                    }
                });

                builder.create().show();
			}
		}		
		);
		

		Beginconnect.setOnClickListener(
		new OnClickListener(){
			public void onClick(View v){
	            // TODO Auto-generated method stub  
				if (choose == 0){
                    Intent intent = new Intent(FirstPageActivity.this,ServerActivity.class);
                    startActivity(intent);
                }
                else if (choose == 1){
                    Intent intent =new Intent(FirstPageActivity.this,ClientActivity.class);
                    startActivity(intent);
                }
                else{
                    Toast toast =Toast.makeText(getApplicationContext(),"请先在设置中选择用户类型",Toast.LENGTH_LONG);
                    toast.show();
                }
			}
		}		
		);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.first_page, menu);
		return true;
	}
	
	

}
