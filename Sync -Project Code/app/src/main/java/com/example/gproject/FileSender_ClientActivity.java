package com.example.gproject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.network.NetTcpFileReceiveThread;
import com.poi.poiandroid.R;

import java.util.AbstractCollection;

/**
 * Created by Joe on 2016/2/24 0007.
 */
public class FileSender_ClientActivity extends Activity{


    private Button receivebutton;
    private EditText IpAd;
    private String filename;

    public String commonfile_savepath;

    protected  void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.sendfile_clientpage);

        receivebutton =(Button)findViewById(R.id.receiveaddres);

        Intent i = getIntent();
        Uri uri = i.getData();
//        String path = uri.getPath();

        receivebutton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String path = null;
                        String[] fileType = {"all"};//要过滤的文件类型列表
                        FileChooser dlg = new FileChooser(FileSender_ClientActivity.this, 1, fileType, path, filelistener);
                        dlg.setTitle("选择文件路径");
                        dlg.show();
                    }
                }
        );
    }

    final FileListener filelistener = new FileListener(){

        public void refreshActivity(String text){

            TextView file_receivepath =(TextView) findViewById(R.id.file_receivepath);
            file_receivepath.setText(text);
            NetTcpFileReceiveThread.FilerSender_ChangePath(text);

        }

        public void pptOpen(String fm) {
            // TODO Auto-generated method stub

            filename = fm;
            showDialog(FileSender_ClientActivity.this);
        }

    };


    //创建线程用于接收文件
    private void RecFileLink(ProgressDialog m_Dialog){
        String pathArray[] =new String[1] ;

        String ip=(String) IpAd.getText().toString();
        Thread fileReceiveThread = new Thread(new NetTcpFileReceiveThread("0",ip,pathArray,filename,m_Dialog));	//�½�һ�������ļ��߳�
        fileReceiveThread.start();//启动线程
        Log.i("hi,","in Filesender_client");


    }

    private  void showDialog(Context context){
        ProgressDialog m_Dialog = ProgressDialog.show(FileSender_ClientActivity.this, "请等待...", "正在接受文件，请稍后",true);
        RecFileLink(m_Dialog);
    }



    //通过读取TextView来读取选中文件路径;
    public String getFilePath()
    {
        TextView pathView =(TextView) findViewById(R.id.pathView);
        String fpath=(String)pathView.getText();
        Log.d("路径", fpath);
        return  fpath;

    }


}
