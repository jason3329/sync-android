package com.example.gproject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.network.NetTcpFileSendThread;
import com.example.network.SocketServer;
import com.example.network.WifiAdmin;
import com.poi.poiandroid.R;

import java.net.SocketException;

/**
 * Created by Zuxin on 2016/2/22 0024.
 */
public class FileSenderActivity extends Activity{
    private Button choosefilebutton;
    private Button sendfilebutton;

    private SocketServer fileserver = ServerActivity.server;

    private TextView IpText;
    private WifiAdmin wifi;

    protected  void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.sendfile_page);


        choosefilebutton = (Button) findViewById(R.id.FileChooseButton);
        sendfilebutton = (Button) findViewById(R.id.FileSendButton);
        IpText =ServerActivity.IpText;
        wifi=new WifiAdmin(FileSenderActivity.this);

//        buildfileServer();



        choosefilebutton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String path = null;
                        String[] fileType = {"all"};//要过滤的文件类型列表
                        FileChooser dlg = new FileChooser(FileSenderActivity.this, 1, fileType, path, filelistener);
                        dlg.setTitle("Choose dst file dir");
                        dlg.show();
                    }

                }
        );

        sendfilebutton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ProgressDialog file_dialog = ProgressDialog.show(FileSenderActivity.this,"请等待...", "正在传输文件，请稍后...",true);
                        String filename = getFileName(getFilePath());
                        SendFileLink(file_dialog);
                        fileserver.sendMsg("file:" +filename);
                    }
                }
        );
    }





    //创建发送文件线程
        private void SendFileLink(ProgressDialog file_Dialog){
            String pathArray[] =new String[1] ;
            int clientnum = fileserver.ClientSize();
            pathArray[0]="/mnt"+getFilePath();

            //if(SendFileThread != null)  { SendFileThread.close(); SendFileThread=null; }
            NetTcpFileSendThread SendFileThread = new NetTcpFileSendThread(pathArray,file_Dialog,clientnum);
            //netTcpFileSendThread.start();	//启动线程
        }


        final FileListener filelistener = new FileListener(){

        public void refreshActivity(String text){

            TextView filepathView =(TextView) findViewById(R.id.filepathView);
            filepathView.setText(text);

        }

        public void pptOpen(String fm) {
            // TODO Auto-generated method stub

        }



        };
    public String getFileName(String pathandname){

        int start=pathandname.lastIndexOf("/");
        if(start!=-1 ){
            return pathandname.substring(start+1);
        }else{
            return null;
        }


    }

    public String getFilePath()
    {
        TextView filepathView =(TextView) findViewById(R.id.filepathView);

        String fpath=(String)filepathView.getText();
        Log.d("路径", fpath);
        return  fpath;

    }

//    //创建服务器;
//    private void buildfileServer(){
//        if(fileserver == null)
//        { fileserver=new SocketServer(filelistener);
//            fileserver.createServer();
//
//            Toast.makeText(getApplicationContext(), "成功创建服务器", Toast.LENGTH_SHORT).show();
//
//
//            String s = "";
//            try {
//                s=wifi.getLocalIP4Address();
//            } catch (SocketException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//
//            IpText.append(s);
//
//        }
//    }

    public void onDestroy() {
        super.onDestroy();

        if(fileserver!=null) {fileserver.close(); fileserver = null;}

        //if(SendFileThread != null)  { SendFileThread.close(); SendFileThread=null; }


    }






}
