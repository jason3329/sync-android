package com.control;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.network.SocketServer;
import com.poi.poiandroid.R;

import org.apache.commons.logging.impl.WeakHashtable;
import org.apache.xmlbeans.impl.common.ConcurrentReaderHashMap;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;


public class ClientVoAdapter extends BaseAdapter {
    protected Context mContext;
    protected List<SocketServer.ClientVo> mDatas;
    protected LayoutInflater mInflater;
    private int layoutId;

    public Map<SocketServer.ClientVo, Boolean> powercheck;


    public ClientVoAdapter(Context mContext, List<SocketServer.ClientVo> mDatas,
                           int layoutId, Map<SocketServer.ClientVo, Boolean> powercheck) {
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);
        this.mDatas = mDatas;
        this.layoutId = layoutId;
        this.powercheck = powercheck;
        if (powercheck == null || powercheck.size() == 0) {
            for (int i = 0; i < mDatas.size(); i++) {
                powercheck.put(mDatas.get(i), false);
            }
        }
    }

    public ClientVoAdapter(Context context, List<SocketServer.ClientVo> datas, int layoutId) {
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
        this.mDatas = datas;
//        for (int i = 0; i < mDatas.size(); i++) {
//            powercheck.put(mDatas.get(i), false);
//        }
        this.layoutId = layoutId;
    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public SocketServer.ClientVo getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;


        if (convertView == null) {
            convertView = mInflater.inflate(layoutId, null);
            holder = new ViewHolder();
            holder.ListView_ip = (TextView) convertView.findViewById(R.id.listview_ip);
            holder.getListView_nickname = (TextView) convertView.findViewById(R.id.listview_nickname);
            holder.write_control = (android.widget.CheckBox) convertView.findViewById(R.id.control_checkbox);

            convertView.setTag(holder);//绑定ViewHolder对象
        } else {
            holder = (ViewHolder) convertView.getTag(); //取出ViewHolder对象
        }

        holder.ListView_ip.setText(mDatas.get(position).getIp());
        holder.getListView_nickname.setText(mDatas.get(position).getName());

        holder.write_control.setChecked(powercheck.get(mDatas.get(position)));
        holder.write_control.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                powercheck.put(mDatas.get(position), isChecked);
            }
        });

        return convertView;
    }

    public Map<SocketServer.ClientVo, Boolean> getPowercheck() {
        return powercheck;
    }

}
